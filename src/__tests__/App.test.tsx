import CurrencyItem from "../components/CurrencyItem";
import CurrencySelector from "../components/CurrencySelector";
import { currencies } from "../constants";
import { fireEvent, render, cleanup } from "@testing-library/react";

describe(CurrencyItem, () => {
  afterEach(cleanup);
  it("should ensure all currencies are displayed", () => {
    const handleCurrencyToggle = jest.fn();
    const { getByTestId } = render(
      <CurrencyItem
        index={0}
        key={0}
        currency={currencies[0]}
        onClick={() => () => {
          handleCurrencyToggle({ name: "EUR" });
        }}
        isSelected={true}
      />
    );
    const currencyItem = getByTestId("currency-item-0").textContent;
    expect(currencyItem).toBe("EUR");
  });

  it("should ensure there are no currencies in the start", () => {
    const { getByTestId } = render(<CurrencySelector />);
    const selectItem = getByTestId("selected-currencies").textContent;
    expect(selectItem).toBe("");
  });

  it("should add currency to the list", () => {
    const { getAllByTestId, getByTestId } = render(<CurrencySelector />);
    const currencyItems = getAllByTestId("currency-item-1");
    const firstCurrencyItem = currencyItems[0];
    fireEvent.click(firstCurrencyItem);
    const selectItem = getByTestId("selected-currency").textContent;
    expect(selectItem).toBe("pln");
  });

  it("should toggle the currency selector", () => {
    const { getAllByTestId, getByTestId } = render(<CurrencySelector />);
    const currencyItems = getAllByTestId("currency-item-1");
    const firstCurrencyItem = currencyItems[0];
    fireEvent.click(firstCurrencyItem);
    const selectedCurrencies = getByTestId("selected-currencies").textContent;
    expect(selectedCurrencies).toBe("");
  });

  it("should remove the currency from the list when clicked on cross icon", () => {
    const { getByTestId, getAllByTestId } = render(<CurrencySelector />);
    const currencyItems = getAllByTestId("currency-item-1");
    const firstCurrencyItem = currencyItems[0];
    fireEvent.click(firstCurrencyItem);
    const deleteBtn = getByTestId("delete-btn");
    fireEvent.click(deleteBtn);
    const selectedCurrencies = getByTestId("selected-currencies").textContent;
    expect(selectedCurrencies).toBe("");
  });
});
