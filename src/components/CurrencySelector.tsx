import { useCallback, useEffect, useState } from "react";
import { currencies } from "../constants";
import { Currency } from "../types";
import CurrencyItem from "./CurrencyItem";

const CurrencySelector = () => {
  const initialSelected = () => {
    const storedCurrencies = sessionStorage.getItem("currencies");
    return storedCurrencies ? JSON.parse(storedCurrencies) : [];
  };

  const [selected, setSelected] = useState<Currency[]>(initialSelected);

  const handleCurrencyToggle = useCallback(
    (currency: Currency) => {
      const isSelected = selected.some(
        (selectedCurrency) => selectedCurrency.name === currency.name
      );

      if (isSelected) {
        setSelected((prevSelected) =>
          prevSelected.filter(
            (selectedCurrency) => selectedCurrency.name !== currency.name
          )
        );
      } else {
        setSelected((prevSelected) => [...prevSelected, currency]);
      }
    },
    [selected]
  );

  useEffect(() => {
    sessionStorage.setItem("currencies", JSON.stringify(selected));
  }, [selected]);

  return (
    <div className="currency-selector">
      <div
        data-testid="selected-currencies"
        className="selected-currencies currency-items"
      >
        {selected.length > 0 &&
          selected.map((currency: Currency, index: number) => (
            <div
              key={index}
              className="selected-currency currency"
              data-testid="chosen-currency"
            >
              <span data-testid="selected-currency">
                {currency.name.toLocaleLowerCase()}
              </span>
              <button
                data-testid="delete-btn"
                className="unselect-currency"
                onClick={() => handleCurrencyToggle(currency)}
              >
                <span>x</span>
              </button>
            </div>
          ))}
      </div>
      <div className="currency-items">
        {currencies.map((currency: Currency, index: number) => (
          <CurrencyItem
            key={index}
            index={index}
            currency={currency}
            onClick={() => handleCurrencyToggle(currency)}
            isSelected={selected.some(
              (selectedCurrency) => selectedCurrency.name === currency.name
            )}
          />
        ))}
      </div>
    </div>
  );
};

export default CurrencySelector;
