import { Currency } from "../types";

interface CurrencyProps {
  index: number;
  currency: Currency;
  onClick: () => void;
  isSelected: boolean;
}

const CurrencyItem = ({
  currency,
  onClick,
  isSelected,
  index,
}: CurrencyProps) => {
  return (
    <div
      onClick={onClick}
      data-testid={`currency-item-${index}`}
      className={`currency ${isSelected && "selected"}`}
    >
      <input readOnly checked={isSelected} type="checkbox" />
      <span>{currency.name}</span>
    </div>
  );
};

export default CurrencyItem;
